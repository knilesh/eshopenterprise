Application.$controller("ProfilePageController", ["$scope", "$rootScope", "Widgets", "Variables",
    function($scope, $rootScope, Widgets, Variables) {
        "use strict";

        /** This is a callback function, that is called after a page is rendered.*/
        $scope.onPageReady = function () {
            /*
             * widgets can be accessed through 'Widgets' service here
             * e.g. Widgets.byId(), Widgets.byName()or access widgets by Widgets.widgetName
             */
            $rootScope.pageLoading = false;
            $rootScope.$safeApply($rootScope);
        };

        /**This is a callback function, that is called after the page variables are loaded. */
        $scope.onPageVariablesReady = function () {
            /*
             * variables can be accessed through 'Variables' service here
             * e.g. Variables.staticVariable1.getData()
             */
        };

        /** On updating the profile data, we update the userObject with the modified data and display messages
         * based on the response*/
        $scope.saveClick = function($event, $scope) {
            Widgets.successSaveMsg.show = false;
            Widgets.failureSaveMsg.show = false;

            var data = Variables.currentUser.dataSet;
            data.name = Widgets.NameVal.datavalue;
            data.landmark = Widgets.LandmarkVal.datavalue;
            data.country = Widgets.CountryVal.datavalue;
            data.street = Widgets.StreetVal.datavalue;
            data.phone = Widgets.PhoneVal.datavalue;
            data.pin = Widgets.PinVal.datavalue;
            data.city = Widgets.CityVal.datavalue;
            Variables.currentUser.dataSet = data;

            Variables.call("updateRow", "users", {
                "row": data,
                "id": data.id
            }, function(response) {
                /** Showing success or failure message*/
                if (response.error) {
                    Widgets.failureSaveMsg.show = true;
                } else {
                    Widgets.successSaveMsg.show = true;
                }
            });

        };
    }
]);